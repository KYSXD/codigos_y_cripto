# Reed-Solomon

Implementación de prueba para curso Ene-Jun 2023

## Para instalar

Instalar dependencias

```bash
$ pip install numpy
$ pip install galois
```

Se recomienda tener instalado `pipenv`
e instalar todas las dependencias en un ambiente virtual

```bash
$ pipenv install
```

## Para correr

Correr con la bandera de ayuda o con los argumentos que exige

```bash
$ python rs.py --help
$ python rs.py -m 3 2 1 -q 929 -n 20 -e 3 -a POW --log-level INFO
```

Se recomienda tener instalado `pipenv`
y correr desde el entorno creado

```bash
$ pipenv run python rs.py --help
$ pipenv run python rs.py -m 3 2 1 -q 929 -n 20 -e 3 -a POW --log-level INFO
```
