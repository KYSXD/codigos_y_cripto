import argparse


def extract_initial_config():
    parser = argparse.ArgumentParser(
        description='RS decoder',
    )
    parser.add_argument(
        "-q",
        **{
            'action': "store",
            'type': int,
            'help': "alphabet size",
            'default': 929,
        },
    )
    parser.add_argument(
        "-n",
        **{
            'action': "store",
            'type': int,
            'help': "block lenght",
            'default': 10,
        },
    )
    parser.add_argument(
        "-m",
        "--message",
        **{
            'action': "store",
            'type': int,
            'nargs': '+',
            'help': "message to encode",
            'required': True,
        },
    )
    parser.add_argument(
        "-e",
        **{
            'action': "store",
            'type': int,
            'help': "errors allowed",
            'default': 3,
        },
    )
    parser.add_argument(
        "-a",
        **{
            'action': "store",
            'choices': ['SEQ', 'POW'],
            'help': "evaluation points",
            'default': 'SEQ',
        },
    )
    parser.add_argument(
        "--log-level",
        **{
            'action': "store",
            'choices': ['DEBUG', 'INFO'],
            'help': "logger level",
            'default': 'DEBUG',
        },
    )
    args = parser.parse_args()
    return vars(args)
