import galois


def validate_args(config):
    q = config['q']
    if not galois.is_prime(q):
        msg = (
            f'{q} is not prime'
            ' (Only prime supported due to current implementation)'
        )
        raise ValueError(msg)

    message = config['message']
    if any(q <= c for c in message):
        msg = f'All symbols must be within [0, {q})'
        raise ValueError(msg)

    n = config['n']
    k = len(config['message'])
    if not k < n:
        msg = f'Message length must be within [0, {n})'
        raise ValueError(msg)

    if not n <= q:
        msg = f'Block size must be within [0, {q}]'
        raise ValueError(msg)

    e = config['e']
    if not e <= n:
        msg = f'Errors must be within [0, {n}]'
        raise ValueError(msg)
